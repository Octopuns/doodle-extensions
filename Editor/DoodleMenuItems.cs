﻿namespace DoodleStudio95.Extensions
{
#if UNITY_EDITOR
	using System.Collections;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine;
	using System.IO;

	/// <summary>
	/// Collection of menu items for compressing, decompressing and generating spritesheets from DoodleAnimationFiles
	/// </summary>
	public class DoodleMenuItems
	{
		#region Compress
		[MenuItem("Assets/DoodleExtensions/Compress Doodle Animation File/DTX5", priority = 1)]
		private static void CompressDoodleAnimationFile()
		{
			foreach (Object obj in Selection.objects)
			{
				DoodleAnimationFile doodleAnimationFile = obj as DoodleAnimationFile;

				if (doodleAnimationFile == null)
				{
					Debug.LogWarning("Selected object is not a valid DoodleAnimationFile.");
				}
				else
				{
					DoodleExtensions.CompressDoodleAnimationFile(doodleAnimationFile, TextureFormat.DXT5, TextureCompressionQuality.Best);
				}
			}
		}

		[MenuItem("Assets/DoodleExtensions/Compress Doodle Animation File/DTX5", true)]
		private static bool CompressDoodleAnimationFileValidation()
		{
			foreach (Object obj in Selection.objects)
			{

				// This returns true when the selected object is a Variable (the menu item will be disabled otherwise).
				if (Selection.activeObject as DoodleAnimationFile == null)
					return false;
			}

			return true;
		}
		#endregion

		#region Crunch Compress
		[MenuItem("Assets/DoodleExtensions/Compress Doodle Animation File/DTX5Crunched", priority = 1)]
		private static void CrunchDoodleAnimationFile()
		{
			foreach (Object obj in Selection.objects)
			{
				DoodleAnimationFile doodleAnimationFile = obj as DoodleAnimationFile;

				if (doodleAnimationFile == null)
				{
					Debug.LogWarning("Selected object is not a valid DoodleAnimationFile.");
				}
				else
				{
					DoodleExtensions.CompressDoodleAnimationFile(doodleAnimationFile, TextureFormat.DXT5Crunched, TextureCompressionQuality.Best);
				}
			}
		}

		[MenuItem("Assets/DoodleExtensions/Compress Doodle Animation File/DTX5Crunched", true)]
		private static bool CrunchDoodleAnimationFileValidation()
		{
			foreach (Object obj in Selection.objects)
			{

				// This returns true when the selected object is a Variable (the menu item will be disabled otherwise).
				if (Selection.activeObject as DoodleAnimationFile == null)
					return false;
			}

			return true;
		}
		#endregion

		#region ETC2 Compress
		[MenuItem("Assets/DoodleExtensions/Compress Doodle Animation File/ETC2_RGBA8", priority = 1)]
		private static void CompressETC2DoodleAnimationFile()
		{
			foreach (Object obj in Selection.objects)
			{
				DoodleAnimationFile doodleAnimationFile = obj as DoodleAnimationFile;

				if (doodleAnimationFile == null)
				{
					Debug.LogWarning("Selected object is not a valid DoodleAnimationFile.");
				}
				else
				{
					DoodleExtensions.CompressDoodleAnimationFile(doodleAnimationFile, TextureFormat.ETC2_RGBA8, TextureCompressionQuality.Best);
				}
			}
		}

		[MenuItem("Assets/DoodleExtensions/Compress Doodle Animation File/ETC2_RGBA8", true)]
		private static bool CompressETC2DoodleAnimationFileValidation()
		{
			foreach (Object obj in Selection.objects)
			{

				// This returns true when the selected object is a Variable (the menu item will be disabled otherwise).
				if (Selection.activeObject as DoodleAnimationFile == null)
					return false;
			}

			return true;
		}
		#endregion

		#region Decompress
		[MenuItem("Assets/DoodleExtensions/Decompress Doodle Animation File", priority = 1)]
		private static void DecompressDoodleAnimationFile()
		{
			foreach (Object obj in Selection.objects)
			{
				DoodleAnimationFile doodleAnimationFile = obj as DoodleAnimationFile;

				if (doodleAnimationFile == null)
				{
					Debug.LogWarning("Selected object is not a valid DoodleAnimationFile.");
				}
				else
				{
					DoodleExtensions.DecompressDoodleAnimationFile(doodleAnimationFile, TextureFormat.RGBA32);
				}
			}

		}

		[MenuItem("Assets/DoodleExtensions/Decompress Doodle Animation File", true)]
		private static bool DecompressDoodleAnimationFileValidation()
		{
			foreach (Object obj in Selection.objects)
			{

				// This returns true when the selected object is a Variable (the menu item will be disabled otherwise).
				if (Selection.activeObject as DoodleAnimationFile == null)
					return false;
			}

			return true;
		}
		#endregion

		#region Spritesheet
		[MenuItem("Assets/DoodleExtensions/Generate Spritesheet", priority = 1)]
		private static void GenerateSpritesheet()
		{
			foreach (Object obj in Selection.objects)
			{
				DoodleAnimationFile doodleAnimationFile = obj as DoodleAnimationFile;

				if (doodleAnimationFile == null)
				{
					Debug.LogWarning("Selected object is not a valid DoodleAnimationFile.");
				}
				else
				{

					string path = AssetDatabase.GetAssetPath(doodleAnimationFile);
					string directory = Path.GetDirectoryName(path);
					string fileName = Path.GetFileNameWithoutExtension(path);

					string spritesheetFilePath = directory + "/" + fileName + "-spritesheet.png";

					List<Texture2D> textures = new List<Texture2D>();
					foreach (DoodleAnimationFileKeyframe keyframe in doodleAnimationFile.frames)
						textures.Add(keyframe.Texture);

					int rows = 0;
					int columns = 0;

					Texture2D spritesheet = DoodleExtensions.CreateSpritesheet(spritesheetFilePath, textures.ToArray(), true, out rows, out columns);
				}
			}
		}

		[MenuItem("Assets/DoodleExtensions/Generate Spritesheet", true)]
		private static bool GenerateSpritesheetValidation()
		{ 
			foreach (Object obj in Selection.objects)
			{
				// This returns true when the selected object is a Variable (the menu item will be disabled otherwise).
				if (Selection.activeObject as DoodleAnimationFile == null)
					return false;
			}

			return true;
		}
		#endregion

		#region Doodle Animation File From SpriteSheet (Not Yet Implemented)
		//[MenuItem("Assets/DoodleExtensions/Generate Doodle Animation From Spritesheet")]
		private static void GenerateDoodleAnimationFile()
		{
			foreach (Object obj in Selection.objects)
			{
				Texture2D spritesheet = obj as Texture2D;

				if (spritesheet == null)
				{
					Debug.LogWarning("Selected object is not a valid spritesheet");
				}
				else
				{

					string path = AssetDatabase.GetAssetPath(spritesheet);
					string directory = Path.GetDirectoryName(path);
					string fileName = Path.GetFileNameWithoutExtension(path);

					string animationFilePath = directory + "/" + fileName + "-animationfile.asset";

					int rows = 2;
					int columns = 2;
					int textureCount = 0;

					Texture2D[] textures = DoodleExtensions.UnpackTextures(spritesheet, rows, columns, textureCount);

					DoodleAnimationFile animationFile = DoodleExtensions.CreateAnimationFile(textures, animationFilePath);
				}
			}
		}

		//[MenuItem("Assets/DoodleExtensions/Generate Doodle Animation From Spritesheet", true)]
		private static bool GenerateDoodleAnimationFileValidation()
		{
			foreach (Object obj in Selection.objects)
			{
				// This returns true when the selected object is a Variable (the menu item will be disabled otherwise).
				if (Selection.activeObject as Texture2D == null)
					return false;
			}

			return true;
		}
		#endregion
	}

#endif
}
