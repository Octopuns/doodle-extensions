﻿namespace DoodleStudio95.Extensions
{
#if UNITY_EDITOR
	using System.Collections;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine;
	using System.IO;

	/// <summary>
	/// Collection of functions that help when dealing with DoodleStudio files
	/// </summary>
	public class DoodleExtensions
	{
		/// <summary>
		/// Convert a sprite sheet to a DoodleAnimationFile
		/// </summary>
		public static DoodleAnimationFile CreateAnimationFile(Texture2D[] textures, string path)
		{
			//get path directory
			string directory = Path.GetDirectoryName(path);

			//create a new animation file and frames list
			DoodleAnimationFile newAnimationFile = ScriptableObject.CreateInstance<DoodleAnimationFile>();
			newAnimationFile.frames = new List<DoodleAnimationFileKeyframe>();

			//for every passed in texture, add it as a keyframe in the doodle animation file and as a sub-asset (texture & sprite)
			for (int i = 0; i < textures.Length; i++)
			{
				//create a keyframe and sprite
				DoodleAnimationFileKeyframe keyframe = new DoodleAnimationFileKeyframe(1, textures[i], false);
				keyframe.Sprite = keyframe.CreateSprite();

				AssetDatabase.AddObjectToAsset(textures[i], newAnimationFile);
				AssetDatabase.AddObjectToAsset(keyframe.Sprite, newAnimationFile);

				newAnimationFile.frames.Add(keyframe);
			}

			//create the animation file
			DoodleAnimationFile animationFileAsset = CreateAsset<DoodleAnimationFile>(newAnimationFile, path);

			//refresh the asset database
			AssetDatabase.Refresh();

			//return the created animation file asset
			return animationFileAsset;
		}

		/// <summary>
		/// Overwrite the DoodleAnimationFile asset with the passed in textures. This should keep scene references. Returns true if the animationFile textures
		/// were successfully over-written.
		/// </summary>
		public static bool ReplaceTextures( DoodleAnimationFile animationFile, Texture2D[] textures )
		{
			//if the original file is null
			if (animationFile == null)
			{
				Debug.LogError("No original file specified.");
				return false;
			}

			//get the path and directory of the original animation file
			string path = AssetDatabase.GetAssetPath(animationFile);
			string directory = Path.GetDirectoryName(path);

			//create a new animation file and frames list
			DoodleAnimationFile newAnimationFile = ScriptableObject.CreateInstance<DoodleAnimationFile>();
			newAnimationFile.frames = new List<DoodleAnimationFileKeyframe>();

		
			//remove all existing sub-assets from the animation file
			Object[] assetObjects = AssetDatabase.LoadAllAssetsAtPath(path);

			foreach (Object o in assetObjects)
			{
				if (!AssetDatabase.IsMainAsset(o))
					AssetDatabase.RemoveObjectFromAsset(o);
			}
			
			AssetDatabase.Refresh();

			//copy the new file's data over the original file
			EditorUtility.CopySerialized(newAnimationFile, animationFile);
			
			//for every passed in texture, add it as a keyframe in the doodle animation file and as a sub-asset (texture & sprite)
			for (int i = 0; i < textures.Length; i++)
			{
				//create a keyframe and sprite
				DoodleAnimationFileKeyframe keyframe = new DoodleAnimationFileKeyframe(1, textures[i], false);
				keyframe.Sprite = keyframe.CreateSprite();

				AssetDatabase.AddObjectToAsset(textures[i], animationFile);
				AssetDatabase.AddObjectToAsset(keyframe.Sprite, animationFile);

				animationFile.frames.Add(keyframe);
			}

			//refresh the asset database
			AssetDatabase.Refresh();

			return true;
		}

		/// <summary>
		/// Create a spritesheet at path by packing textures into a single texture. Fills rows and cols
		/// with the amount of rows and columns (of textures) in the resulting texture. If enforceSquares is true, the
		/// spritesheet will be created with the same amount of rows and columns. For example, with enforceSquares set to true
		/// and 3 textures passed in - the resulting spritesheet will have 2 rows and 2 columns (with one blank).
		/// </summary>
		public static Texture2D CreateSpritesheet(string path, Texture2D[] textures, bool enforceSquares, out int rows, out int cols)
		{
			//initialise rows and columns to default values
			rows = 1;
			cols = 1;

			//rows and columns
			Texture2D packedAtlas = PackTextures(textures, enforceSquares, false, out rows, out cols);
			packedAtlas.Apply();

			//write spritesheet as png and then import it using the AssetDatabase
			byte[] pngBytes = packedAtlas.EncodeToPNG();
			File.WriteAllBytes(path, pngBytes);
			AssetDatabase.ImportAsset(path);

			//load it to ensure saving was successful
			Texture2D spriteSheet = AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;
			return spriteSheet;
		}

		/// <summary>
		/// Pack the passed in textures into a single texture.
		/// </summary>
		public static Texture2D PackTextures(Texture2D[] textures, bool enforceSquare, bool convertToWhite, out int rows, out int columns)
		{
			int textureWidth = textures[0].width;
			int textureHeight = textures[0].height;

			int textureCount = textures.Length;

			if (enforceSquare)
			{
				//set rows and columns
				rows = (Mathf.CeilToInt(Mathf.Sqrt(textureCount)));
				columns = rows;
			}
			else
			{
				//set rows and columns
				rows = (Mathf.FloorToInt(Mathf.Sqrt(textureCount)));
				columns = rows;
				int spots = rows * columns;

				if (textureCount % spots == 0)
				{
					columns *= textureCount / spots;
				}
				else
				{
					columns += Mathf.CeilToInt((textureCount - spots) / (float)rows);
				}
			}

			//now we know the desired rows and columns
			Texture2D packedTexture = new Texture2D(columns * textureWidth, rows * textureHeight);

			for (int t = 0; t < textures.Length; t++)
			{
				//copy each texture in
				int row = Mathf.FloorToInt(t / columns);
				int col = t - row * columns;

				int pixelOffsetX = col * textureWidth;
				int pixelOffsetY = row * textureHeight;

				int packedTexturePixelCount = packedTexture.GetPixels().Length;

				//for each pixel in the smaller textures
				for (int x = 0; x < textures[t].width; x++)
				{
					for (int y = 0; y < textures[t].height; y++)
					{
						int pixelX = pixelOffsetX + x;
						int pixelY = pixelOffsetY + y;
						Color texturePixel = textures[t].GetPixel(x, y);

						if (convertToWhite)
						{
							texturePixel.r = 1.0f;
							texturePixel.g = 1.0f;
							texturePixel.b = 1.0f;
						}

						packedTexture.SetPixel(pixelX, pixelY, texturePixel);
					}
				}
			}


			return packedTexture;
		}

		/// <summary>
		/// Unpack a uniform (each sprite is the same width and height) spritesheet into separate textures
		/// </summary>
		public static Texture2D[] UnpackTextures(Texture2D spritesheet, int rows, int columns, int textureCount)
		{
			List<Texture2D> unpackedTextures = new List<Texture2D>();

			int singleTextureWidth = (int)(spritesheet.width / (float)columns);
			int singleTextureHeight = (int)(spritesheet.height / (float)rows);

			for (int t = 0; t < textureCount; t++)
			{
				//read each texture in
				int row = Mathf.FloorToInt(t / columns);
				int col = t - row * columns;

				//how much to jump to start at this texture's position
				int pixelOffsetX = col * singleTextureWidth;
				int pixelOffsetY = (row * singleTextureHeight);

				Texture2D subTexture = new Texture2D(singleTextureWidth, singleTextureHeight, spritesheet.format, false);

				//for each pixel in the smaller textures
				for (int x = 0; x < subTexture.width; x++)
				{
					for (int y = 0; y < subTexture.height; y++)
					{
						int pixelX = pixelOffsetX + x;
						int pixelY = pixelOffsetY + y;
						Color texturePixel = spritesheet.GetPixel(pixelX, pixelY);
						subTexture.SetPixel(x, y, texturePixel);
					}
				}

				unpackedTextures.Add(subTexture);
			}

			return unpackedTextures.ToArray();
		}


		/// <summary>
		/// Create asset at path and return the asset created if successful
		/// </summary>
		public static T CreateAsset<T>(T asset, string path) where T : UnityEngine.Object
		{
			AssetDatabase.CreateAsset(asset, path);
			AssetDatabase.Refresh();

			T createdAsset = AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
			return createdAsset;
		}

		/// <summary>
		/// Create asset at path or replace it if the asset already exists
		/// </summary>
		public static T CreateOrReplaceAsset<T>(T asset, string path) where T : Object
		{
			T existingAsset = AssetDatabase.LoadAssetAtPath<T>(path);

			if (existingAsset == null)
			{
				AssetDatabase.CreateAsset(asset, path);
				existingAsset = asset;
			}
			else
			{
				EditorUtility.CopySerialized(asset, existingAsset);
			}

			return asset;
		}

		/// <summary>
		/// Compress the doodle animation file
		/// </summary>
		public static void CompressDoodleAnimationFile(DoodleAnimationFile animationFile, TextureFormat compressionFormat, TextureCompressionQuality compressionQuality)
		{
			// Get all the textures attached to the frames
			List<Texture2D> textures = new List<Texture2D>();
			foreach (DoodleAnimationFileKeyframe keyframe in animationFile.frames)
				textures.Add(keyframe.Texture);

			// compress each texture
			foreach (Texture2D tex in textures)
				EditorUtility.CompressTexture(tex, compressionFormat, compressionQuality);

			AssetDatabase.SaveAssets();
		}

		/// <summary>
		/// Decompress the Doodle Animation File
		/// </summary>
		public static void DecompressDoodleAnimationFile(DoodleAnimationFile animationFile, TextureFormat textureFormat, bool mipmaps = false)
		{

				// get all the textures attached to the frames
				List<Texture2D> textures = new List<Texture2D>();
				foreach (DoodleAnimationFileKeyframe keyframe in animationFile.frames)
					textures.Add(keyframe.Texture);

				List<Texture2D> newTextures = new List<Texture2D>();

				// create new textures
				foreach (Texture2D tex in textures)
				{
					Texture2D newTexture = new Texture2D(tex.width, tex.height, textureFormat, mipmaps);

					Color32[] pixels = tex.GetPixels32();
					newTexture.SetPixels32(pixels);
					newTexture.Apply();
					newTextures.Add(newTexture);
				}

				//// overwrite the doodle animation file using the new textures
				if ( ReplaceTextures(animationFile, newTextures.ToArray()) )
				{
					string resultFilePath = AssetDatabase.GetAssetPath(animationFile);
					AssetDatabase.ImportAsset(resultFilePath);
					AssetDatabase.SaveAssets();
				}
	


		}

	}
#endif
}
