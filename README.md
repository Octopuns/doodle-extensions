# README #

### Overview ###
Set of small editor utility functions to help when working with Doodle Studio 95. Contains functions for compression, decompression, spritesheet packing and unpacking and others.
Adds menu items to the right-click menu for common compression formats and spritesheet generation (that fixes the bug in Doodle Studio 95 that generates spritesheets too wide).

### Quick Start ###

* Download source or [package](https://bitbucket.org/Octopuns/doodle-extensions/downloads/DoodleExtensions_2019.3.8f1.unitypackage) of latest version of doodle-extensions
* Import package into Unity (and ensure scripts are inside an Editor folder)
* Right click on any Doodle Animation File or Texture 2D and select Doodle Extensions to view menu items.

### Notes ###

* This has been tested with Unity 2019.3.8f1 (64-bit) running on a Windows 10 PC.

### Known Issues ###

* DoodleAnimationFile generation menu item is not yet enabled as I don't currently have a way of auto-determining rows and columns from a spritesheet
* Compression/Decompression will sometimes fail 
* Compressing textures before build seems to cause issues on Android with very slow load times (likely decompressing at runtime because the format is wrong)

### Who do I talk to? ###

* Direct all queries to octopuns@gmail.com. I'd love to hear what you think.